<?php

/**
 * @file
 * Contains hook_post_update_NAME() implementations.
 */

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * Implements hook_post_update_NAME().
 *
 * Searches all entity bundles for fields implementing of type typed resource
 * object. For each, updates the view modes converting formatter type from
 * text_default to basic_string.
 *
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function jsonapi_reference_post_update_legacy_field_formatter_settings(&$sandbox) {
  /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager */
  $entityTypeManager = \Drupal::service('entity_type.manager');

  // Get all fieldable entity definitions.
  $entityTypeDefinitions = array_filter(
    $entityTypeManager->getDefinitions(),
    function (EntityTypeInterface $entityType) {
      return $entityType->entityClassImplements(FieldableEntityInterface::class);
    }
  );

  /** @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo */
  $entityTypeBundleInfo = \Drupal::service('entity_type.bundle.info');

  // Get the bundle information for all fieldable entity definitions.
  $entityBundleMap = [];
  foreach ($entityTypeDefinitions as $entityTypeDefinition) {
    $bundleInfo = $entityTypeBundleInfo->getBundleInfo($entityTypeDefinition->id());
    $entityBundleMap[$entityTypeDefinition->id()] = array_keys($bundleInfo);
  }

  $fieldConfigStorage = $entityTypeManager->getStorage('field_config');

  $entityUpdateMap = [];

  // Look through all the bundles for typed_resource_object fields.
  foreach ($entityBundleMap as $entity => $bundles) {
    foreach ($bundles as $bundle) {
      $bundleFieldConfig = $fieldConfigStorage->loadByProperties([
        'entity_type' => $entity,
        'bundle' => $bundle,
      ]);
      /** @var \Drupal\field\Entity\FieldConfig $fieldConfig */
      foreach ($bundleFieldConfig as $fieldConfig) {
        if ($fieldConfig->getType() != 'typed_resource_object') {
          // We don't care about this field.
          continue;
        }
        // This entity & bundle combination has a typed_resource_object field.
        // We need to adjust any display settings for it. For now, remember the
        // entity, bundle, and field.
        if (!array_key_exists($entity, $entityUpdateMap)) {
          $entityUpdateMap[$entity] = [];
        }
        if (!array_key_exists($bundle, $entityUpdateMap[$entity])) {
          $entityUpdateMap[$entity][$bundle] = [];
        }
        $entityUpdateMap[$entity][$bundle][] = $fieldConfig->getName();
      }
    }
  }

  /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entityDisplayRepository */
  $entityDisplayRepository = \Drupal::service('entity_display.repository');

  foreach ($entityUpdateMap as $entity => $bundleFieldMap) {
    $viewModes = [EntityDisplayRepositoryInterface::DEFAULT_DISPLAY_MODE];
    $viewModes = array_merge($viewModes, array_keys($entityDisplayRepository->getViewModes($entity)));
    foreach ($bundleFieldMap as $bundle => $fields) {
      foreach ($viewModes as $viewName) {
        $viewDisplay = $entityDisplayRepository->getViewDisplay($entity, $bundle, $viewName);
        $displayChanged = FALSE;
        foreach ($fields as $field) {
          if ($component = $viewDisplay->getComponent($field)) {
            if ($component['type'] == 'basic_string') {
              $component['type'] = 'text_default';
              $viewDisplay->setComponent($field, $component);
              $displayChanged = TRUE;
            }
          }
        }
        if ($displayChanged) {
          $viewDisplay->save();
        }
      }
    }
  }

}
