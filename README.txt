JSON:API reference provides a field type Typed Resource Object, that is similar
to an entity reference field. However, rather than refer to an entity on the
same Drupal installation, this refers to a resource object exposed via
JSON:API.

For example - if you are syndicating content from one system to another, you
may want to create a typed resource object field on the user entity type on
the source system, which refers to the corresponding user on the destination
system.

You then may want to add a typed resource object field to the content types
from which you are syndicating on the source system so that you can record the
relationship to the newly created content in the future.

The module provides an autocomplete widget that looks up resources in the
remote system as you type and suggests . Because different types of resource
object will autocomplete by comparing the user input against a different
attribute, the attribute against which autocompletion takes place is configured
in the widget.

Caveats & Assumptions
* The module assumes that all fields come from the same remote system 
* The module assumes that authentication is done by basic authentication on the
  remote system
* The module has not been tested against non-Drupal remote systems, but this
  may work
* The module does not provide and Field Formatters so currently displays the
  GUID of the resource object as text
