<?php

declare(strict_types=1);

namespace Drupal\Tests\jsonapi_reference\Functional;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\SchemaCheckTestTrait;

/**
 * Base class for functional tests.
 *
 * Provides base functionality to perform various tests with the
 * object_type_resource field type.
 */
abstract class JsonApiReferenceFieldBase extends BrowserTestBase {
  use SchemaCheckTestTrait, StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'jsonapi_reference',
    'field_ui',
    'node',
    'jsonapi',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The name of the test content type.
   *
   * @var string
   */
  protected $testContentType = 'test_content_type';

  /**
   * The machine name of the field type.
   *
   * @var string
   */
  protected $fieldType = 'typed_resource_object';

  /**
   * The field config.
   *
   * @var \Drupal\field\Entity\FieldConfig
   */
  protected $fieldDefinition;

  /**
   * The name of the field to test.
   *
   * @var string
   */
  protected $fieldName;

  /**
   * A user with administrative capabilities.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->config('jsonapi_reference.settings')
      ->set('endpoint', Url::fromUserInput('/jsonapi')->setAbsolute()->toString())
      ->set('username', '')
      ->set('password', '')
      ->save();

    // Create a content type to test with.
    $this->drupalCreateContentType(['type' => $this->testContentType]);

    // Rebuild the routes before performing any tests to ensure the JSON:API
    // dynamic routes function correctly.
    \Drupal::service('router.builder')->rebuild();

    // Generate a random field name to test the typed_resource_object field
    // type. The field is then created using the generated name and the config
    // is asserted as existing.
    $this->fieldName = 'field_' . mb_strtolower($this->randomMachineName());

    /** @var \Drupal\field\Entity\FieldStorageConfig $field_storage */
    $field_storage = FieldStorageConfig::create([
      'field_name' => $this->fieldName,
      'entity_type' => 'node',
      'type' => $this->fieldType,
      'settings' => [
        'resource_object_type' => 'node--' . $this->testContentType,
      ],
    ]);
    $field_storage->save();

    /** @var \Drupal\field\Entity\FieldConfig $field */
    $this->fieldDefinition = FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => $this->testContentType,
    ]);
    $this->fieldDefinition->save();

    $this->setDisplaySettings();

    $field_config = $this->config('field.field.node.' . $this->testContentType . '.' . $this->fieldName);
    $this->assertConfigSchema(\Drupal::service('config.typed'), $field_config->getName(), $field_config->get());
    $field_storage_config = $this->config('field.storage.node.' . $this->fieldName);
    $this->assertConfigSchema(\Drupal::service('config.typed'), $field_storage_config->getName(), $field_storage_config->get());

    // Create an admin user capable of configuring and creating a field using
    // the 'Typed resource object' field type.
    $this->adminUser = $this->createUser([
      'configure jsonapi reference',
      'access content',
      'administer node fields',
      'administer node form display',
      'administer content types',
      'bypass node access',
    ]);
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Set the display style of the field.
   *
   * @param string $display_style
   *   The display style to set.
   * @param string $attribute
   *   Component display setting attribute.
   */
  protected function setDisplaySettings(string $display_style = 'label', string $attribute = 'title') {
    \Drupal::entityTypeManager()
      ->getStorage('entity_view_display')
      ->load('node.' . $this->testContentType . '.default')
      ->setComponent($this->fieldName, [
        'type' => 'typed_resource_object_string',
        'label' => 'above',
        'settings' => [
          'display_style' => $display_style,
          'attribute' => $attribute,
        ],
        'third_party_settings' => [],
        'region' => 'content',
        'weight' => 1,
      ])
      ->save();
  }

}
