<?php

declare(strict_types=1);

namespace Drupal\Tests\jsonapi_reference\Functional;

/**
 * Functional tests for the field formatter.
 *
 * @group jsonapi_reference.
 */
class JsonApiReferenceFieldFormatterTest extends JsonApiReferenceFieldBase {

  /**
   * The item of content to reference.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $referencedContent;

  /**
   * The item of content referencing $referencedContent.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $referenceContent;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create an item of content to be referenced.
    $this->referencedContent = $this->createNode([
      'type' => $this->testContentType,
    ]);

    $this->referenceContent = $this->createNode([
      'type' => $this->testContentType,
      $this->fieldName => ['value' => $this->referencedContent->uuid()],
    ]);
  }

  /**
   * Test the 'GUID' display style.
   */
  public function testGuid() {
    $this->setDisplaySettings('guid');

    $this->drupalGet('node/' . $this->referenceContent->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains($this->referencedContent->uuid());

    $this->drupalLogout();
  }

  /**
   * Test the 'Label' display style.
   */
  public function testLabel() {
    $this->setDisplaySettings('label');

    $this->drupalGet('node/' . $this->referenceContent->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains($this->referencedContent->label());

    $this->drupalLogout();
  }

  /**
   * Test the 'Label (GUID)' display style.
   */
  public function testLabelGuid() {
    $this->setDisplaySettings('label_id');

    $this->drupalGet('node/' . $this->referenceContent->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains($this->referencedContent->label() . ' (' . $this->referencedContent->uuid() . ')');

    $this->drupalLogout();
  }

  /**
   * Test the formatter setting 'label_attribute'.
   */
  public function testLabelAttributeSetting() {
    $this->setDisplaySettings('label', 'title');
    $this->drupalGet('node/' . $this->referenceContent->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains("<div>{$this->referencedContent->getTitle()}</div>");

    $this->setDisplaySettings('label', 'drupal_internal__nid');
    $this->drupalGet('node/' . $this->referenceContent->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains("<div>{$this->referencedContent->id()}</div>");

    $this->drupalLogout();
  }

}
