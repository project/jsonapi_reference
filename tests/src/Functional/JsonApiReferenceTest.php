<?php

declare(strict_types=1);

namespace Drupal\Tests\jsonapi_reference\Functional;

use Drupal\KernelTests\AssertConfigTrait;
use Drupal\Tests\BrowserTestBase;

/**
 * Functional tests for JsonApiReference.
 *
 * @group jsonapi_reference
 */
class JsonApiReferenceTest extends BrowserTestBase {
  use AssertConfigTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['jsonapi_reference'];

  /**
   * Test configuration form protected by permissions.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testConfigFormProtected() {
    // Test anonymous users can't access the configuration screen.
    $this->drupalGet('admin/config/jsonapi_reference');
    $this->assertSession()->statusCodeEquals(403);

    // Test a logged in user with no specific permissions can't access the
    // configuration screen.
    $person_of_no_significance = $this->createUser();
    $this->drupalLogin($person_of_no_significance);
    $this->drupalGet('admin/config/jsonapi_reference');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogout();

    // Test a logged in user with the appropriate permission can access the
    // configuration screen.
    $person_of_appropriate_significance = $this->createUser(['configure jsonapi reference']);
    $this->drupalLogin($person_of_appropriate_significance);
    $this->drupalGet('admin/config/jsonapi_reference');
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalLogout();
  }

}
