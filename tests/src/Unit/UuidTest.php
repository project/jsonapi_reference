<?php

declare(strict_types=1);

namespace Drupal\Tests\jsonapi_reference\Unit;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\jsonapi_reference\Plugin\Field\FieldWidget\TypedResourceObjectAutocompleteWidget;
use Drupal\Tests\UnitTestCase;

/**
 * Tests UUID processing in the widget.
 *
 * @group jsonapi_reference
 */
class UuidTest extends UnitTestCase {

  /**
   * Widget to test.
   *
   * @var \Drupal\jsonapi_reference\Plugin\Field\FieldWidget\TypedResourceObjectAutocompleteWidget
   */
  protected $widget;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $field_definition = $this->prophesize(FieldDefinitionInterface::class);
    $this->widget = new TypedResourceObjectAutocompleteWidget('typed_resource_object_autocomplete', [], $field_definition->reveal(), [], []);
  }

  /**
   * Tests the Uuid can be found from the widget's input.
   */
  public function testUuidLocation() {
    $form_state = $this->prophesize(FormStateInterface::class);
    $form_state->isRebuilding()->willReturn(FALSE);

    // Test a title with the UUID at the end.
    $result = $this->widget->valueCallback([], 'Node Title (d76037b2-2ec0-11ed-a261-0242ac120002)', $form_state->reveal());
    self::assertEquals('d76037b2-2ec0-11ed-a261-0242ac120002', $result);

    // Test the UUID can still be found when part of the title is in
    // parentheses.
    $result = $this->widget->valueCallback([], 'Node (Title) (d76037b2-2ec0-11ed-a261-0242ac120002)', $form_state->reveal());
    self::assertEquals('d76037b2-2ec0-11ed-a261-0242ac120002', $result);

    // Test lots more parentheses.
    $result = $this->widget->valueCallback([], 'Hi (thing) (parentheses) (d76037b2-2ec0-11ed-a261-0242ac120002)', $form_state->reveal());
    self::assertEquals('d76037b2-2ec0-11ed-a261-0242ac120002', $result);

    // Test that the last UUID is the one returned if the title happens to be a
    // UUID.
    $result = $this->widget->valueCallback([], '(591ef6e4-2ec1-11ed-a261-0242ac120002) (d76037b2-2ec0-11ed-a261-0242ac120002)', $form_state->reveal());
    self::assertEquals('d76037b2-2ec0-11ed-a261-0242ac120002', $result);

    // Test what happens if there is only a UUID.
    $result = $this->widget->valueCallback([], '(d76037b2-2ec0-11ed-a261-0242ac120002)', $form_state->reveal());
    self::assertEquals('d76037b2-2ec0-11ed-a261-0242ac120002', $result);
  }

}
