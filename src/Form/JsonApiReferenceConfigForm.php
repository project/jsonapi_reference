<?php

namespace Drupal\jsonapi_reference\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class to provide a form for configuring the module.
 */
class JsonApiReferenceConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'jsonapi_reference.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'json_api_reference_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('jsonapi_reference.settings');
    $form['entrypoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Entrypoint'),
      '#description' => $this->t('The root JSON:API entrypoint, for example, https://example.com/jsonapi'),
      '#maxlength' => 2048,
      '#size' => 64,
      '#default_value' => $config->get('endpoint'),
    ];
    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#description' => $this->t('Username for authenticating on the JSON:API entrypoint'),
      '#maxlength' => 128,
      '#size' => 64,
      '#default_value' => $config->get('username'),
    ];
    $form['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#description' => $this->t('Password for authenticating on the JSON:API entrypoint'),
      '#maxlength' => 128,
      '#size' => 64,
      '#default_value' => $config->get('password'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    $config = $this->config('jsonapi_reference.settings');
    $config->set('endpoint', $form_state->getValue('entrypoint'))
      ->set('username', $form_state->getValue('username'));
    if ($password = $form_state->getValue('password')) {
      $config->set('password', $password);
    }
    $config->save();
  }

}
