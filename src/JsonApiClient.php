<?php

namespace Drupal\jsonapi_reference;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;

/**
 * Client for accessing data through JSON:API.
 *
 * @package Drupal\jsonapi_reference
 */
class JsonApiClient implements JsonApiClientInterface {

  /**
   * Entrypoint to call to retrieve JSONAPI objects.
   */
  protected string $entrypoint;

  /**
   * Username to use when authenticating to the entrypoint.
   */
  protected string $username;

  /**
   * Password to use when authenticating to the entrypoint.
   */
  protected string $password;

  /**
   * JsonApiClient constructor.
   *
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   Client for querying the JSON:API entrypoint.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $loggerChannel
   *   Channel for logging.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   */
  public function __construct(
    protected readonly ClientInterface $httpClient,
    protected readonly LoggerChannelInterface $loggerChannel,
    ConfigFactoryInterface $configFactory,
  ) {
    $this->entrypoint = $configFactory->get('jsonapi_reference.settings')->get('endpoint') ?? '';
    $this->username = $configFactory->get('jsonapi_reference.settings')->get('username') ?? '';
    $this->password = $configFactory->get('jsonapi_reference.settings')->get('password') ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function listResourceObjectTypes(): array {
    return array_keys($this->getResourceObjectTypes());
  }

  /**
   * Gets the resource object type information from the entrypoint.
   *
   * @return object[]
   *   Array of objects, keyed by type name (EG node--article).
   *   Each object contains a href element pointing to the JSON:API entrypoint
   *   for that specific type.
   */
  protected function getResourceObjectTypes(): array {
    try {
      $response = $this->httpClient->request('get', $this->entrypoint, $this->getRequestOptions());
    }
    catch (GuzzleException $e) {
      $this->loggerChannel->error($e->getMessage());
      return [];
    }
    $response = json_decode($response->getBody()->getContents());
    if (!isset($response->links)) {
      $this->loggerChannel->error('JSON:API entrypoint @entrypoint returned no type information.', ['@entrypoint' => $this->entrypoint]);
      return [];
    }
    return (array) $response->links;
  }

  /**
   * {@inheritdoc}
   */
  public function search($resource_object_type, $search_attribute, $query): array {
    $path = $this->getPathForResourceObjectType($resource_object_type);
    if (!$path) {
      $this->loggerChannel->error('No path for resource object type @type found at JSON:API entrypoint @entrypoint.', [
        '@type' => $resource_object_type,
        '@entrypoint' => $this->entrypoint,
      ]);
      return [];
    }

    // Note the x is of no special significance, but JSON:API filter conditions
    // need to be grouped by a name.
    $options = [
      RequestOptions::QUERY => [
        'filter[x][condition][path]' => $search_attribute,
        'filter[x][condition][operator]' => 'CONTAINS',
        'filter[x][condition][value]' => $query,
      ],
    ] + $this->getRequestOptions();
    try {
      $response = $this->httpClient->request('get', $path, $options);
    }
    catch (GuzzleException $e) {
      $this->loggerChannel->error($e->getMessage());
      return [];
    }
    $response = json_decode($response->getBody()->getContents());
    if (!isset($response->data)) {
      return [];
    }
    $results = [];
    foreach ($response->data as $result) {
      $results[] = [$result->attributes->$search_attribute, $result->id];
    }
    $this->loggerChannel->error('JSON:API entrypoint @entrypoint returned no data for path @path when searching for objects where the @search_attribute attribute matched "@query".', [
      '@entrypoint' => $this->entrypoint,
      '@path' => $path,
      '@search_attribute' => $search_attribute,
      '@query' => $query,
    ]);

    return $results;
  }

  /**
   * {@inheritdoc}
   */
  public function searchById(string $resource_object_type, string $id): object|NULL {
    $path = $this->getPathForResourceObjectType($resource_object_type);
    if (!$path) {
      return NULL;
    }
    $path .= "/$id";

    try {
      $response = $this->httpClient->request('get', $path, $this->getRequestOptions());
    }
    catch (GuzzleException $e) {
      $this->loggerChannel->error($e->getMessage());
      return NULL;
    }

    return json_decode($response->getBody()->getContents());
  }

  /**
   * Get common HTTP request options.
   *
   * @return array
   *   Array of request options, including headers and auth.
   */
  protected function getRequestOptions(): array {
    $options = [
      RequestOptions::HEADERS => [
        'Content-Type' => 'application/json',
        'Accept' => 'application/json',
      ],
    ];
    if ($this->username && $this->password) {
      $options[RequestOptions::AUTH] = [
        $this->username,
        $this->password,
      ];
    }

    return $options;
  }

  /**
   * Gets the URI for retrieving a given object type.
   *
   * @param string $resource_object_type
   *   Resource object type.
   *
   * @return string
   *   URI for the given object type.
   *   Empty string if none found.
   */
  protected function getPathForResourceObjectType(string $resource_object_type): string {
    $types = $this->getResourceObjectTypes();

    if (!array_key_exists($resource_object_type, $types)) {
      return '';
    }
    if (!isset($types[$resource_object_type]->href)) {
      return '';
    }
    return $types[$resource_object_type]->href;
  }

}
