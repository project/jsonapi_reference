<?php

namespace Drupal\jsonapi_reference\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\jsonapi_reference\JsonApiClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class to provide a controller for autocomplete look-ups.
 *
 * @package Drupal\jsonapi_reference\Controller
 */
class TypedResourceController extends ControllerBase {

  /**
   * TypedResourceController constructor.
   *
   * @param \Drupal\jsonapi_reference\JsonApiClientInterface $client
   *   Client to talk to our JSON:API entrypoint.
   */
  public function __construct(
    protected readonly JsonApiClientInterface $client,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): TypedResourceController {
    return new static($container->get('jsonapi_reference.jsonapi_client'));
  }

  /**
   * Controller for the autocomplete request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Incoming HTTP request.
   * @param string $resource_object_type
   *   JSON:API Object type for which we are searching, EG node--article.
   * @param string $autocomplete_attribute
   *   Attribute on the JSON:API objects against which we are searching.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   JsonResponse containing array of search results.
   *   Each element in the array contains a value and label showing the matching
   *   values of the autocomplete attribute, and the corresponding objects
   *   JSON:API ID.
   */
  public function autocomplete(Request $request, string $resource_object_type, string $autocomplete_attribute): JsonResponse {
    $response = [];
    $query = $request->query->get('q');
    $search_results = $this->client->search($resource_object_type, $autocomplete_attribute, $query);
    foreach ($search_results as $search_result) {
      $attribute = $search_result[0];
      $id = $search_result[1];
      $response[] = [
        'value' => "$attribute ($id)",
        'label' => "$attribute ($id)",
      ];
    }
    return new JsonResponse($response);
  }

}
