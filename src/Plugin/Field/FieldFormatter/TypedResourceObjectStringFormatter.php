<?php

namespace Drupal\jsonapi_reference\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\jsonapi_reference\JsonApiClientInterface;
use Drupal\jsonapi_reference\Plugin\Field\FieldType\TypedResourceObjectItem;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of a 'typed_resource_object_string' formatter.
 *
 * @FieldFormatter(
 *   id = "typed_resource_object_string",
 *   label = @Translation("JSON:API attribute"),
 *   field_types = {
 *     "typed_resource_object"
 *   }
 * )
 */
class TypedResourceObjectStringFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs a new TypedResourceObjectStringFormatter.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Third party settings.
   * @param \Drupal\jsonapi_reference\JsonApiClientInterface $jsonapiClient
   *   The JSON:API reference client.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, protected JsonApiClientInterface $jsonapiClient) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): TypedResourceObjectStringFormatter {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('jsonapi_reference.jsonapi_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'display_style' => 'label',
      'attribute' => 'title',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $elements = parent::settingsForm($form, $form_state);

    $elements['display_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Display style'),
      '#options' => $this->getResourceObjectDisplayOptions(),
      '#default_value' => $this->getSetting('display_style'),
      '#required' => TRUE,
    ];

    $elements['attribute'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Attribute'),
      '#default_value' => $this->getSetting('attribute'),
      '#description' => $this->t('The attribute in the JSON:API resource object to display.'),
      '#required' => TRUE,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = parent::settingsSummary();

    $display_styles = $this->getResourceObjectDisplayOptions();
    $summary[] = $this->t('Display style: @display_style', ['@display_style' => $display_styles[$this->getSetting('display_style')]]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];

    $display_style = $this->getSetting('display_style');

    // Load the field settings to determine how the field should be displayed.
    foreach ($items as $delta => $item) {
      $elements[$delta] = $this->formatDisplayStyle($item, $display_style);
    }

    return $elements;
  }

  /**
   * Return an array of available string-based display styles for the field.
   *
   * @return array
   *   The array of available field display styles.
   */
  protected function getResourceObjectDisplayOptions(): array {
    return [
      'label' => $this->t('Attribute'),
      'label_id' => $this->t('Attribute (ID)'),
    ];
  }

  /**
   * Format the output for the given display style.
   *
   * To override this function and provide a custom string-based display style,
   * remember to call `parent::formatDisplayStyle`.
   *
   * @param \Drupal\jsonapi_reference\Plugin\Field\FieldType\TypedResourceObjectItem $item
   *   The field item.
   * @param string $display_style
   *   The machine-readable display style.
   *
   * @return array
   *   The element's output.
   */
  protected function formatDisplayStyle(TypedResourceObjectItem $item, string $display_style): array {
    $response = $this->getResourceObjectResponse($item);

    switch ($display_style) {
      case 'label':
        if (isset($response->data->attributes->{$this->getSetting('attribute')})) {
          $element = [
            '#type' => 'inline_template',
            '#template' => '{{ value|nl2br }}',
            '#context' => ['value' => $response->data->attributes->{$this->getSetting('attribute')}],
          ];
        }
        else {
          $element = $this->defaultDisplayStyleValue($item);
        }
        break;

      case 'label_id':
        if (isset($response->data->attributes->{$this->getSetting('attribute')})) {
          $element = [
            '#type' => 'inline_template',
            '#template' => '{{ value|nl2br }}',
            '#context' => [
              'value' => $this->t('@title (@id)', [
                '@title' => $response->data->attributes->{$this->getSetting('attribute')},
                '@id' => $item->value,
              ]),
            ],
          ];
        }
        else {
          $element = $this->defaultDisplayStyleValue($item);
        }
        break;

      default:
        $element = $this->defaultDisplayStyleValue($item);
        break;
    }

    return $element;
  }

  /**
   * Provide the default display style value.
   *
   * @param \Drupal\jsonapi_reference\Plugin\Field\FieldType\TypedResourceObjectItem $item
   *   The field item.
   *
   * @return array
   *   The default display style array.
   */
  protected function defaultDisplayStyleValue(TypedResourceObjectItem $item): array {
    return [
      '#type' => 'inline_template',
      '#template' => '{{ value|nl2br }}',
      '#context' => ['value' => $item->value],
    ];
  }

  /**
   * Use the JSON:API reference client to retrieve data.
   *
   * @param \Drupal\jsonapi_reference\Plugin\Field\FieldType\TypedResourceObjectItem $item
   *   The field item.
   *
   * @return object|null
   *   See \Drupal\jsonapi_reference\JsonApiClientInterface::searchById().
   */
  protected function getResourceObjectResponse(TypedResourceObjectItem $item): object|NULL {
    $object_type = $item->getFieldDefinition()->getSetting('resource_object_type');
    return $this->jsonapiClient->searchById($object_type, $item->value);
  }

}
