<?php

namespace Drupal\jsonapi_reference\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\OptionsProviderInterface;
use Drupal\jsonapi_reference\JsonApiClientInterface;

/**
 * Plugin implementation of the 'typed_resource_object' field type.
 *
 * @FieldType(
 *   id = "typed_resource_object",
 *   label = @Translation("Typed resource object"),
 *   description = @Translation("Field to represent a resource object in a remote system with a specific type."),
 *   category = "reference",
 *   default_formatter = "typed_resource_object_string",
 *   default_widget = "typed_resource_object_autocomplete"
 * )
 */
class TypedResourceObjectItem extends FieldItemBase implements OptionsProviderInterface {

  /**
   * The JSON:API client service.
   */
  private JsonApiClientInterface $client;

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings(): array {
    return [
      'resource_object_type' => '',
      'title_attribute' => '',
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {
    // Prevent early t() calls by using the TranslatableMarkup.
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('ID'))
      ->setSetting('case_sensitive', FALSE)
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {
    return [
      'columns' => [
        'value' => [
          'type' => 'varchar_ascii',
          'length' => 128,
          'binary' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints(): array {
    return [\Drupal::typedDataManager()->getValidationConstraintManager()->create('Uuid', [])];
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition): array {
    $values['value'] = \Drupal::service('uuid')->generate();
    $values['title_attribute'] = 'title';
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data): array {
    $elements = [];

    $types = $this->client()->listResourceObjectTypes();

    $elements['resource_object_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#default_value' => $this->getSetting('resource_object_type'),
      '#required' => TRUE,
      '#description' => $this->t('The type of resource object.'),
      '#disabled' => $has_data,
      '#options' => array_combine($types, $types),
    ];

    $elements['title_attribute'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title attribute'),
      '#default_value' => $this->getSetting('title_attribute'),
      '#required' => TRUE,
      '#description' => $this->t('The name of the JSON:API attribute on the resource object that represents the title of this resource object type. For example, for resource objects representing Drupal nodes, this will be title. For taxonomy terms, it will be name.'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function isEmpty(): bool {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * Gets the JSON:API client service.
   *
   * @return \Drupal\jsonapi_reference\JsonApiClientInterface
   *   The JSON:API client service.
   */
  protected function client(): JsonApiClientInterface {
    if (!isset($this->client)) {
      $this->client = \Drupal::service('jsonapi_reference.jsonapi_client');
    }

    return $this->client;
  }

  /**
   * Automatically cast field to a string.
   *
   * This is necessary for the Uuid Constraint Validator.
   *
   * @return string
   *   String value of the Uuid.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function __toString() {
    return $this->get('value')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function getPossibleValues(?AccountInterface $account = NULL): array {
    return $this->getSettableValues($account);
  }

  /**
   * {@inheritdoc}
   */
  public function getPossibleOptions(?AccountInterface $account = NULL): array {
    return $this->getSettableOptions($account);
  }

  /**
   * {@inheritdoc}
   */
  public function getSettableValues(?AccountInterface $account = NULL): array {
    return array_keys($this->getSettableOptions($account));
  }

  /**
   * {@inheritdoc}
   */
  public function getSettableOptions(?AccountInterface $account = NULL): array {
    $options = $this->client()->search(
      $this->getSetting('resource_object_type'),
      $this->getSetting('title_attribute'),
      NULL
    );

    $return = [];
    foreach ($options as $option) {
      $return[$option[1]] = $option[0];
    }

    return $return;
  }

}
