<?php

namespace Drupal\jsonapi_reference\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'typed_resource_object_autocomplete' widget.
 *
 * @FieldWidget(
 *   id = "typed_resource_object_autocomplete",
 *   label = @Translation("Autocomplete"),
 *   description = @Translation("An autocomplete field."),
 *   field_types = {
 *     "typed_resource_object"
 *   }
 * )
 */
class TypedResourceObjectAutocompleteWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'autocomplete_attribute' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $element['autocomplete_attribute'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Autocomplete attribute'),
      '#default_value' => $this->getAutocompleteAttribute(),
      '#description' => $this->t('The attribute in the JSON:API resource object against which to match when auto-completing.'),
      '#required' => FALSE,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $field = $items->getFieldDefinition();
    $field_label = $field->getLabel();
    $field_settings = $field->getSettings();

    $default_value = NULL;
    if (isset($items[$delta]->value)) {
      $default_value = $items[$delta]->value;
    }
    $element['value'] = $element + [
      '#type' => 'textfield',
      '#title' => $field_label,
      '#default_value' => $default_value,
      '#value_callback' => [get_class($this), 'valueCallback'],
    ];
    // Only add autocomplete attributes if field settings are defined.
    // This prevents Drupal from ever trying to construct an invalid route.
    if ($field_settings['resource_object_type']) {
      $autocomplete_attribute = $this->getAutocompleteAttribute();
      $element['value']['#autocomplete_route_name'] = 'jsonapi_reference.typed_resource_controller_autocomplete';
      $element['value']['#autocomplete_route_parameters'] = [
        'resource_object_type' => $field_settings['resource_object_type'],
        'autocomplete_attribute' => $autocomplete_attribute,
      ];
      $element['value']['#resource_object_type'] = $field_settings['resource_object_type'];
      $element['value']['#autocomplete_attribute'] = $autocomplete_attribute;
    }

    return $element;
  }

  /**
   * Determines how user input is mapped to an element's #value property.
   *
   * @param array $element
   *   An associative array containing the properties of the element.
   * @param mixed $input
   *   The incoming input to populate the form element. If this is FALSE,
   *   the element's default value should be returned.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return mixed
   *   The value to assign to the element.
   */
  public static function valueCallback(array $element, mixed $input, FormStateInterface $form_state): mixed {
    $value = '';

    // If we're rebuilding the form due to AJAX (for example, adding another
    // item to the field), just keep the input as is.
    if ($form_state->isRebuilding() && $input) {
      return $input;
    }

    // If input is being submitted..
    if ($input !== FALSE) {
      // .. convert this to an ID to store.
      $opening_bracket_index = strrpos($input, '(');
      $closing_bracket_index = strrpos($input, ')');
      if ($opening_bracket_index === FALSE) {
        return '';
      }
      if ($closing_bracket_index === FALSE) {
        return '';
      }
      if ($opening_bracket_index > $closing_bracket_index) {
        return '';
      }
      return substr($input, $opening_bracket_index + 1, $closing_bracket_index - $opening_bracket_index - 1);
    }

    // Otherwise we need to convert the other way - for the given id..
    if ($element['#default_value']) {
      // ..look it up via jsonapi and format with the autocomplete attribute.
      /** @var \Drupal\jsonapi_reference\JsonApiClientInterface $client */
      $client = \Drupal::service('jsonapi_reference.jsonapi_client');
      $jsonapi_object = $client->searchById($element['#resource_object_type'], $element['#default_value']);
      if ($jsonapi_object) {
        $value = $jsonapi_object->data->attributes->{$element['#autocomplete_route_parameters']['autocomplete_attribute']} . ' (' . $element['#default_value'] . ')';
      }

    }
    return $value;
  }

  /**
   * Gets the name of the appropriate autocomplete JSON:API attribute to use.
   *
   * @return string
   *   Name of the autocomplete attribute.
   *   Will use the field title attribute if no specific autocomplete attribute
   *   has been set.
   */
  protected function getAutocompleteAttribute(): string {
    // If no specific value has been set for this autocomplete widget..
    $autocomplete_attribute = $this->getSetting('autocomplete_attribute');
    if (!$autocomplete_attribute) {
      // ..then use the field's title attribute.
      $autocomplete_attribute = $this->getFieldSetting('title_attribute');
    }
    return $autocomplete_attribute;
  }

}
