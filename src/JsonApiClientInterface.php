<?php

namespace Drupal\jsonapi_reference;

/**
 * Interface for JSON:API clients.
 *
 * @package Drupal\jsonapi_reference
 */
interface JsonApiClientInterface {

  /**
   * Gets a list of resource object types from the remote entrypoint.
   *
   * @return string[]
   *   Array of remote object types.src/JsonApiClient.php
   *   EG. [
   *     "action--action",
   *     "base_field_override--base_field_override",
   *     "block--block",
   *     .
   *     .
   *   ];
   */
  public function listResourceObjectTypes(): array;

  /**
   * Search the entrypoint for a resource object of a given type.
   *
   * @param string $resource_object_type
   *   Resource object type.
   * @param string $search_attribute
   *   Attribute against which we are searching.
   * @param string $query
   *   String for which to search.
   *
   * @return array
   *   Array of search results, where the keys are the values of
   *   $search_attribute containing $query, and the values are the IDs of the
   *   corresponding resource objects.
   */
  public function search(string $resource_object_type, string $search_attribute, string $query): array;

  /**
   * Looks up a resource object of a given type by ID.
   *
   * @param string $resource_object_type
   *   Object type, e.g. node--article.
   * @param string $id
   *   GUID of object we are searching for.
   *
   * @return object|null
   *   Object returned via JSON:API.
   *   Should have
   *   - jsonapi
   *   - data
   *   - links
   *   properties.
   *   Returns NULL if no object found.
   */
  public function searchById(string $resource_object_type, string $id): object|NULL;

}
